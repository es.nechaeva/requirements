#! /usr/bin/python3

import re
import os
import csv
import json
import glob
import argparse
from typing import Tuple, Dict, List


class Requirement:
    def __init__(self, name: str, value: str) -> None:
        self.name  = name
        self.value = value

    def check_value(self, new_value: str) -> bool:
        if self.value and new_value and new_value != self.value:
            return False
        return True

    def is_value_empty(self) -> bool:
        return self.value == ''

    def set_value(self, new_value: str) -> bool:
        if not self.value and new_value:
            self.value = new_value
            return True
        return False


class ReqArray:
    def __init__(self) -> None:
        self.data = {}

    def __find_req__(self, name) -> Requirement:
        req = self.data.get(name, None)
        return req

    def add_req(self, req: Requirement) -> Requirement:
        name = req.name
        cur_req = self.__find_req__(req.name)
        if cur_req and cur_req.is_value_empty():
            cur_req.set_value(req.value)
        elif not cur_req:
            self.data[req.name] = req
            cur_req = req
        elif not cur_req.check_value(req.value):
            raise Exception('Try to add requirement with existed name and new value.\n Current requirement: ({}, {}),\n new requirement ({}, {})'
                            .format(cur_req.name, cur_req.value, name, req.value))
        return cur_req


class Comment:
    def __init__(self, text: str, is_single: bool, line_number: int) -> None:
        self.is_single = is_single
        self.start_line = line_number
        self.comm_value = ''
        self.text_strip(text)

    def text_strip(self, text: str) -> None:
        if self.is_single:
            self.comm_value = text[2:].strip()
        else:
            self.comm_value = re.sub(r'\s', ' ', text[2:-2].strip())

    def get_req(self) -> Requirement:
        if self.comm_value[:5] != '#REQ ':
            return None
        text = self.comm_value[5:].strip()
        req_name = ''
        parts = text.split(' ', 1)
        if parts[0][0] == '#':
            req_name = parts[0][1:].strip()
        if len(parts) == 1:
            req_value = ''
        else:
            req_value = parts[1].strip()
        if not req_name:
            raise Exception('Comment at line {} has contained requirement prefix but name of requirement was not provided.\nComment: {}'
                            .format(self.start_line, self.comm_value))
        return Requirement(req_name, req_value)


class Test:
    def __init__(self, test_line: str, line_number: int, req_array: ReqArray) -> None:
        self.types = ['TEST', 'TEST_F', 'TEST_P', 'TYPED_TEST', 'TYPED_TEST_P']
        self.line_number = line_number
        self.name = ''
        self.suite = ''
        self.type = ''
        self.all_reqs = req_array
        self.test_reqs = []
        self.__parse__(test_line)

    def __parse__(self, test_line: str) -> None:
        text = test_line.strip()
        parts = re.split('[(),]', text)
        parts = [s.strip() for s in parts if s]
        if len(parts) != 3:
            raise Exception('Can not parse string with test name at line {}.\nData: {}'
                            .format(self.line_number, test_line))
        self.type = parts[0]
        self.suite = parts[1]
        self.name = parts[2]
        if not self.type in self.types:
            raise Exception('Unknown type of test at line {}. Test: {}'.format(self.line_number, self.type))

    def add_comment(self, comment: str, is_single: bool, line: int) -> None:
        comment = comment.strip()
        if len(comment) <= 2:
            return
        comm = Comment(comment, is_single, line)
        req = comm.get_req()
        if req:
            try:
                req = self.all_reqs.add_req(req)
            except Exception as e:
                raise Exception('Duplicate requirement at line {}. {}'.format(comm.start_line, str(e)))
            self.test_reqs.append(req)

    def test_to_json(self) -> Dict:
        req_json  = []
        for req in self.test_reqs:
            j_req = {'req_name': req.name,
                     'req_description': req.value}
            req_json.append(j_req)
        test_json = {'test_name': self.name,
                     'suite_name': self.suite,
                     'test_type': self.type,
                     'requirements': req_json}
        return  test_json

    def test_to_text(self) -> str:
        text_lines = ['\t{} {}/{}:'.format(self.type, self.suite, self.name)]
        for req in self.test_reqs:
            text_lines.append('\t\t{}: {}'.format(req.name, req.value))
        return '\n'.join(text_lines)

    def test_to_table(self) -> List:
        # Table header: ['Test_type', 'Test_full_name', 'Req. name', 'Req. description']
        table = []
        for req in self.test_reqs:
            ln = [self.type, '{}/{}'.format(self.suite, self.name), req.name, req.value]
            table.append(ln)
        if not self.test_reqs:
            ln = [self.type, '{}/{}'.format(self.suite, self.name), '', '']
            table.append(ln)
        return table


class TestFile:
    test_template = '^[^\n\s]*TEST_{0,1}[P,F]{0,1}\( *.+, *[\n]* *.+ *\)|^[^\n\s]*TYPED_TEST_{0,1}[P]{0,1}\( *.+, *[\n]* *.+ *\)'
    comm_template = '^[^\n\s]*\t*[^\n\s]*\/\*.+?\*\/$'

    def __init__(self, file_name: str) -> None:
        self.tests = []
        self.all_reqs = ReqArray()
        self.text = ''
        self.active_line = 0
        self.file_name = file_name
        self.__read_file__(file_name)
        self.__parse_file__()

    def __read_file__(self, file_name: str) -> None:
        with open(file_name, 'r') as f:
            self.text = f.read()

    def __find_line_number__(self, line: str, text: str) -> int:
        parts = text.split(line)
        if len(parts) != 2:
            return -1
        line_count = len(parts[0].split('\n'))
        return line_count

    def __get_single_comm__(self, line: str) -> str:
        if line[:2] == '//':
            return line
        return ''

    def __get_multi_line_comment__(self, text: str) -> List:
        multi_comments = re.findall(self.comm_template, text, re.MULTILINE | re.DOTALL)
        return multi_comments

    def __get_line_number_mcomment__(self, comm: str, text: str) -> int:
        line_num = self.__find_line_number__(comm, text)
        return line_num

    def __get_up_comment__(self, up_part: str) -> List:
        text = up_part.strip()
        comm = ''
        is_single = True
        line_num = -1
        if text:
            if text[-2:] == '*/':
                comments = self.__get_multi_line_comment__(text)
                if comments:
                    comm_len = len(comments[-1])
                    if text[-comm_len:] == comments[-1]:
                        is_single = False
                        comm = comments[-1]
            if not comm:
                comm = self.__get_single_comm__(text.split('\n')[-1])
            if comm:
                if is_single:
                    line_num = self.__find_line_number__(comm, up_part) - 1
                else:
                    line_num = self.__get_line_number_mcomment__(comm, up_part)
        if not comm:
            return []
        return [(comm, is_single, line_num)]

    def __body_comments__(self, body: str) -> List:
        comments = []
        m_comments = self.__get_multi_line_comment__(body)
        if m_comments:
            for comm in m_comments:
                line_num = self.__get_line_number_mcomment__(comm, body)
                comments.append((comm, False, line_num))
                parts = body.split(comm)
                if parts[0]:
                    lines = parts[0].split('\n')
                    s_comms = [ln.strip() for ln in lines if len(ln.strip()) > 2 and ln.strip()[:2] == '//']
                    for single_comm in s_comms:
                        line_num = self.__find_line_number__(single_comm, body) - 1
                        comments.append((single_comm, True, line_num))
                body = parts[1]
        lines = body.split('\n')
        s_comms = [ln.strip() for ln in lines if len(ln.strip()) > 2 and ln.strip()[:2] == '//']
        for single_comm in s_comms:
            line_num = self.__find_line_number__(single_comm, body) - 1
            comments.append((single_comm, True, line_num))
        return comments

    def __get_test_body__(self, src: str) -> Tuple:
        src = src.strip()
        if src[0] != '{':
            raise Exception('Can top get function body from text. Expected first symbol from first line \
             is \"{\" but actual symbol is \"{}\". \nLine number: {}, text block:\n {}'.format(src[0], self.active_line, src))
        body_lst = [src[0]]
        cnt = 1
        ind = 1
        is_end_found = False
        for s in src[1:]:
            ind += 1
            if s == '{':
                cnt += 1
            elif s == '}':
                cnt -= 1
            body_lst.append(s)
            if cnt == 0:
                is_end_found = True
            if is_end_found and s == '\n':
                break
        if cnt != 0:
            line_number = self.__find_line_number__(src, self.text)
            raise Exception('Error in syntax of test body at line {}. Text:\n {}'.format(line_number, src))
        return ''.join(body_lst), src[ind:]

    def __parse_file__(self):
        tests_statements = re.findall(self.test_template, self.text, re.MULTILINE)
        self.active_line = 0
        unparsed_text = self.text
        for test_str in tests_statements:
            parts = unparsed_text.split(test_str)
            line_number = self.__find_line_number__(test_str, self.text)
            try:
                cur_test = Test(test_str, line_number + 1, self.all_reqs)
            except:
                #traceback.print_exc()
                print('ERROR: Can not get test name and suite name from string {}'.format(test_str))
                continue
            comments = self.__get_up_comment__(parts[0])
            for comm, is_single, ln in comments:
                try:
                    cur_test.add_comment(comm, is_single, ln + self.active_line)
                except Exception as e:
                    print('ERROR: {}'.format(str(e)))

            self.active_line = line_number + len(test_str.split('\n'))

            try:
                body, unparsed_text = self.__get_test_body__(parts[1])
                comments = self.__body_comments__(body)
            except Exception as e:
                print('ERROR: {}'.format(str(e)))
                break

            for comm, is_single, ln in comments:
                try:
                    cur_test.add_comment(comm, is_single, ln + self.active_line)
                except Exception as e:
                    print('ERROR: {}'.format(str(e)))

            self.active_line += len(body.split('\n')) - 1
            self.tests.append(cur_test)

    def file_to_text(self, res_name: str) -> None:
        if len(self.tests) == 0:
            return
        text = 'File: {}\nTests:\n'.format(self.file_name)
        for test in self.tests:
            text += test.test_to_text() + '\n\n'
        with open(res_name, 'a') as f:
            f.write(text)

    def file_to_json(self) -> Dict:
        if len(self.tests) == 0:
            return {}
        file_json = {'file_name': self.file_name}
        tests = []
        for test in self.tests:
            tests.append(test.test_to_json())
        file_json['tests'] = tests
        return file_json

    def file_to_table(self) -> List:
        # Table header: ['File_name', 'Test_type', 'Test_full_name', 'Req. name', 'Req. description']
        table = []
        for test in self.tests:
            test_table = test.test_to_table()
            for elem in test_table:
                table.append( [self.file_name] + elem)
        return table


class TestDir:
    def __init__(self, dir_name: str) -> None:
        self.dir_name = dir_name
        self.parsed_files = []
        self.files = glob.glob(dir_name + '/**/*.cpp', recursive=True)
        print(self.files)
        self.parse_files()

    def parse_files(self):
        for f in self.files:
            test = TestFile(f)
            self.parsed_files.append(test)

    def data_to_text(self, res_name: str) -> None:
        for fl in self.parsed_files:
            fl.file_to_text(res_name)

    def data_to_json(self, res_name: str) -> None:
        if len(self.parsed_files) == 0:
            return
        data = []
        for fl in self.parsed_files:
            file_json = fl.file_to_json()
            if file_json:
                data.append(file_json)
        with open(res_name, 'w') as fp:
            json.dump(data, fp, indent=1)

    def data_to_table(self, res_name: str) -> None:
        # Table header: ['Dir_path', 'File_name', 'Test_type', 'Test_full_name', 'Req. name', 'Req. description']
        if len(self.parsed_files) == 0:
            return
        table = []
        for fl in self.parsed_files:
            file_table = fl.file_to_table()
            for elem in file_table:
                table.append( [self.dir_name] + elem)

        with open(res_name, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            writer.writerow(['Dir_path', 'File_name', 'Test_type', 'Test_full_name', 'Req. name', 'Req. description'])
            for data in table:
                writer.writerow(data)


def check_file(file_name):
    if not os.path.isfile(file_name):
        print("ERROR: path to file with tests does not exist {}".format(os.path.abspath(os.path.normpath(file_name))))
        exit(1)


def check_dir(dir_name):
    if not os.path.isdir(dir_name):
        print("ERROR: path to folder with tests does not exist {}".format(os.path.abspath(os.path.normpath(dir_name))))
        exit(1)


if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser(
        description='Get statistics of requirements coverage by tests')
    argument_parser.add_argument('--source', '-s', metavar="path_to_tests_file", help='Path to file with tests', default="")
    argument_parser.add_argument('--source_dir', '-d', metavar="path_to_tests_dir", help='Path to directory with tests', default="")
    argument_parser.add_argument('--req', '-r', metavar="path_to_req_file", default="")
    argument_parser.add_argument('--text', '-t', metavar="data.txt", default="data.txt",
                                 help='Path to text file with results')
    argument_parser.add_argument('--json', '-j', metavar="data.json", default="data.json",
                                 help='Path to json file with results. Only for parsing directory')
    argument_parser.add_argument('--csv', '-c', metavar="data.csv", default="data.csv",
                                 help='Path to csv file with results. Only for parsing directory')

    args = argument_parser.parse_args()

    test_fname = args.source
    print(test_fname)
    dir_name = args.source_dir
    print(dir_name)

    if not dir_name and not test_fname:
        print('Need to set source dir or source file for investigation.')
        exit(1)

    if dir_name:
        check_dir(dir_name)
        tests = TestDir(dir_name)

        text_file = args.text
        if text_file:
            tests.data_to_text(text_file)

        json_file = args.json
        if json_file:
            tests.data_to_json(json_file)

        csv_file = args.csv
        if csv_file:
            tests.data_to_table(csv_file)

    else:
        check_file(test_fname)
        file_info = TestFile(test_fname)

        text_file = args.text
        if text_file:
            file_info.file_to_text(text_file)

